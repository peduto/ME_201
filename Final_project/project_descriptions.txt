1. Play-dough rod stability: take play dough (you can make some with a simple recipe if you don't have access to the commercial version), and roll a rod of the play dough to a certain diameter and length. Stand the rod on its end, and observe whether the play-dough `slumps over' as a function of these two geometric parameters. What can you determine about the stability of the playdough rod from your experiments? Can you formulate a predictor for when you expect the rod to `slump,' vs. when it will remain upright? 


2. The capstan problem. Suspend a weight (e.g. a coffee cup) from a rod using a string or twine. Can you use the capstan phenomenon to attain a static equilibrium? How many times must you wrap the string around the rod to keep the coffee cup from falling without additional tension?


3. Fracture of a sheet of paper & notch sensitivity. Suspend a piece of paper from a rigid mount, and affix a clip to the bottom of the paper that you can add weight to (e.g. a binder clip). How much weight can you add before the paper fractures? Now, take a new piece of paper, and cut a horizontal notch in its center. How much weight does it support before breaking? If you double the notch distance, how much weight will it support now?


4. Fracture of a sheet of plastic & plastic deformation. Suspend a sheet of plastic (e.g. cling film) between two grips, and begin attaching weight to the bottom grip. How much weight will the plastic support before it begins to flow? Now, remove the weight and insert a notch in the plastic sheet. Add weight. Does a crack propagate? What do you observe about the material at the fracture surface when the crack propagates?


5. **CAUTION** Convection of oil in a baking dish & patterns of convective flow. Fill a baking dish with cooking oil from the supermarket. Carefully! apply heat to the oil bath. Using small-scale powder (e.g. cinnamon), record the motion of the fluid when viewed from the top and from the side as a function of the amount of heat you apply. Do you observe different motion depending on the strength of the heat source? Do you observe convection? Can you identify a boundary between when the oil will conduct vis-a-vis convect heat?

** Hot oil - make sure when you finish the experiment that you leave the oil in a secure location where it won't spill until it reaches room temperature. You should NEVER try to clean hot oil with water, as it is very dangerous. If you will leave the oil unnattended, be sure to notify anyone (e.g. roommates) who might go near it that it is hot and can severely burn.


6. Friction between a block and surfaces. Take a rigid plane such as a book shelf or stiff cardboard surface. Take a household object such as e.g. a cellphone. Cover the surface of the stiff substrate with different materials, and measure the angle at which sliding sets in. Can you measure the friction coefficient for the object on this surface for different materials using the angle of inclination of the substrate? What do you notice about frictional behavior with different materials? 


7. Flying carpets and lubrication of a sheet of paper - Take an ordinary sheet of paper. Can you pose a trajectory e.g. an initial velocity and orientation, that leads to the longest `glide?' What do you notice about the optimal setting? How does this change when you alter e.g. the weight and size of the paper? What happens when you try this on different surfaces, such as rough asphalt vs. smooth wood / vinyl flooring? How far can the paper glide?


8. Cornstarch and water rheology: Make several mixtures of cornstarch (malzena) and water with different portions of water and conrstarch. Evaluate how these mixtures respond to different strain rates by dropping or throwing the blobs upon the ground. Now, try testing them with slower rates by e.g. pouring the mixtures. What do you notice about the behavior of this mixture? How does its behavior depend on the proportion of cornstarch to water?


9. Strength of wet sand: Take dry sand and pile it up by pouring it through e.g. a funnel or simple cone onto a plate. Now, make the sand `cohesive' by adding some water to it, and repeat the experiment. How tall of a pile can you make? Does this change with the relative proportions of sand and water that you use? Is there an `optimum' amount of water fraction that leads to a `tallest tower?' Can you measure a characteristic geometry as a function of the sand's solid fraction by weight?


10. Coiling honey: build the hightest tower of coiled honey. Play with the jet diameter, the height of deposition and even the temperature of the honey. How does the coil's height change as a function of these parameters? Can you rationalize the dependence you observe on the basis of e.g. the constitutive property of the honey?


11. Hand-made noodles or pasta: stretching and evaluating the elasticity of dough. For the chefs among you, make noodles as shown in the youtube video: . How much does the material strech when the noodles are made? Is the rate of applied stretching important? How stiff are the noodles? Does their stiffness change as they are increasingly stretched? Propose an experiment that allows you to evaluate their constitutive response, and carry out a measurement or two with your proposed experimental configuration.
